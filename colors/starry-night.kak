face global color0 rgb:abb2bf
face global color1 rgb:4889ea
face global color2 rgb:548bcd
face global color3 rgb:b55f5f
face global color4 rgb:e5c07b
face global color5 rgb:a55fbc
face global color6 rgb:6aa512
face global color7 rgb:9dbe5a
face global color8 rgb:d19a66
face global color9 rgb:9da5b4
face global color10 rgb:1e2227
face global color11 rgb:191c21
face global color12 rgb:b6bbc6
face global color13 rgb:143039
face global color14 rgb:363d4d
face global color15 rgb:495162
face global color16 rgb:dcdcdc

# For Code
face global value color8
face global type color6
face global variable color2
face global module color2
face global function color3
face global string color7
face global keyword color1
face global operator color0+b
face global attribute color2
face global comment rgb:80a0ff
face global documentation comment
face global meta color7
face global builtin default+b

# For markup
face global title color1
face global header color3
face global mono color2
face global block color7
face global link color3
face global bullet color3
face global list color6

# builtin faces
face global Default color12,color11
face global PrimarySelection color12,color14
face global SecondarySelection color11,color14
face global PrimaryCursor black,white+fg
face global SecondaryCursor black,white+fg
face global PrimaryCursorEol black,color3+fg
face global SecondaryCursorEol black,color3+fg
face global LineNumbers default,default
face global LineNumberCursor default,default+r
face global MenuForeground color12,color11
face global MenuBackground color11,color12
face global MenuInfo color3
face global Information black,color6
face global Error black,rgb:d19a66
face global DiagnosticError rgb:ffffff,rgb:ff0000
face global DiagnosticWarning rgb:ff0000
face global StatusLine color12,rgb:14171c
face global StatusLineMode color6
face global StatusLineInfo color1
face global StatusLineValue color2
face global StatusCursor black,color3
face global Prompt color6,default
face global MatchingChar default,default+b
face global Whitespace default,default+fd
face global BufferPadding color1,default

# # kak-tree-sitter by phaazon
face global ts_attribute                    attribute
face global ts_comment                      comment
# face global ts_conceal                      "%opt{kts_mauve}+i"
face global ts_constant                     variable
# face global ts_constant_builtin_boolean     "%opt{kts_sky}"
# face global ts_constant_character           "%opt{kts_yellow}"
# face global ts_constant_character_escape    "ts_constant_character"
# face global ts_constant_macro               "%opt{kts_mauve}"
# face global ts_constant_numeric             "%opt{kts_peach}"
# face global ts_constant_numeric_float       "ts_constant_numeric"
# face global ts_constant_numeric_integer     "ts_constant_numeric"
face global ts_constructor                  operator
face global ts_diff_plus                    "%opt{kts_green}"
face global ts_diff_minus                   "%opt{kts_red}"
face global ts_diff_delta                   "%opt{kts_blue}"
face global ts_diff_delta_moved             "%opt{kts_mauve}"
face global ts_error                        DiagnosticError
face global ts_function                     function
face global ts_function_builtin             function
# face global ts_function_macro               "%opt{kts_mauve}"
# face global ts_function_method              "ts_function"
# face global ts_function_special             "ts_function"
face global ts_hint                         documentation
face global ts_info                         documentation
face global ts_keyword                      keyword
# face global ts_keyword_control              "ts_keyword"
# face global ts_keyword_conditional          "%opt{kts_mauve}+i"
# face global ts_keyword_control_conditional  "%opt{kts_mauve}+i"
# face global ts_keyword_control_directive    "%opt{kts_mauve}+i"
# face global ts_keyword_control_import       "%opt{kts_mauve}+i"
# face global ts_keyword_control_repeat       "ts_keyword"
# face global ts_keyword_control_return       "ts_keyword"
# face global ts_keyword_control_except       "ts_keyword"
# face global ts_keyword_control_exception    "ts_keyword"
# face global ts_keyword_directive            "%opt{kts_mauve}+i"
# face global ts_keyword_function             "ts_keyword"
# face global ts_keyword_operator             "ts_keyword"
# face global ts_keyword_special              "ts_keyword" 
# face global ts_keyword_storage              "ts_keyword" 
# face global ts_keyword_storage_modifier     "ts_keyword" 
# face global ts_keyword_storage_modifier_mut "ts_keyword" 
# face global ts_keyword_storage_modifier_ref "ts_keyword" 
# face global ts_keyword_storage_type         "ts_keyword" 
face global ts_label                        attribute
# face global ts_markup_bold                  "%opt{kts_peach}+b"
# face global ts_markup_heading               "%opt{kts_red}"
# face global ts_markup_heading_1             "%opt{kts_red}"
# face global ts_markup_heading_2             "%opt{kts_mauve}"
# face global ts_markup_heading_3             "%opt{kts_green}"
# face global ts_markup_heading_4             "%opt{kts_yellow}"
# face global ts_markup_heading_5             "%opt{kts_pink}"
# face global ts_markup_heading_6             "%opt{kts_teal}"
# face global ts_markup_heading_marker        "%opt{kts_peach}+b"
# face global ts_markup_italic                "%opt{kts_pink}+i"
# face global ts_markup_list_checked          "%opt{kts_green}"
# face global ts_markup_list_numbered         "%opt{kts_blue}+i"
# face global ts_markup_list_unchecked        "%opt{kts_teal}"
# face global ts_markup_list_unnumbered       "%opt{kts_mauve}"
# face global ts_markup_link_label            "%opt{kts_blue}"
# face global ts_markup_link_url              "%opt{kts_teal}+u"
# face global ts_markup_link_uri              "%opt{kts_teal}+u"
# face global ts_markup_link_text             "%opt{kts_blue}"
# face global ts_markup_quote                 "%opt{kts_gray1}"
# face global ts_markup_raw                   "%opt{kts_green}"
# face global ts_markup_raw_block             "%opt{kts_green}"
# face global ts_markup_raw_inline            "%opt{kts_green}"
# face global ts_markup_strikethrough         "%opt{kts_gray1}+s"
# face global ts_namespace                    "%opt{kts_blue}+i"
# face global ts_operator                     "%opt{kts_sky}"
# face global ts_property                     "%opt{kts_sky}"
# face global ts_punctuation                  "%opt{kts_overlay2}"
# face global ts_punctuation_bracket          "ts_punctuation"
# face global ts_punctuation_delimiter        "ts_punctuation"
# face global ts_punctuation_special          "%opt{kts_sky}"
# face global ts_special                      "%opt{kts_blue}"
# face global ts_spell                        "%opt{kts_mauve}"
face global ts_string                       string
# face global ts_string_regex                 "%opt{kts_peach}"
# face global ts_string_regexp                "%opt{kts_peach}"
# face global ts_string_escape                "%opt{kts_mauve}"
# face global ts_string_special               "%opt{kts_blue}"
# face global ts_string_special_path          "%opt{kts_green}"
# face global ts_string_special_symbol        "%opt{kts_mauve}"
# face global ts_string_symbol                "%opt{kts_red}"
# face global ts_tag                          "%opt{kts_mauve}"
# face global ts_tag_error                    "%opt{kts_red}"
face global ts_text                         block
face global ts_text_title                   title
face global ts_type                         type
# face global ts_type_builtin                 "ts_type"
# face global ts_type_enum_variant            "%opt{kts_flamingo}"
face global ts_variable                     variable
# face global ts_variable_builtin             "%opt{kts_red}"
# face global ts_variable_other_member        "%opt{kts_teal}"
# face global ts_variable_parameter           "%opt{kts_maroon}+i"
face global ts_warning                      DiagnosticWarning+b"
